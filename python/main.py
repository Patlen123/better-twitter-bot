import requests
import os
import json
import tweepy as tw
import discord_webhook as dw
import to_discord as td

CONFIG_FILE = open('config.json')


# We don't do everything in main because eventually twit_connect() will also be called through the configurator
def main():
    twit_connect()


def twit_connect():
    # Attempt to read the config file
    try:
        data = json.load(CONFIG_FILE)
        bearer_token = data['twitter']['bearer_token']
        account_to_follow = data['twitter']['account_to_follow']
    except:
        print("There's an issue with the Twitter section of your config file")
        exit(52)

    # Connect to Twitter
    try:
        streaming_client = td.ToDiscord(bearer_token)
        twitter_setup(streaming_client, data)
        print("Connecting to twitter")
        streaming_client.filter()
        print("Connected, waiting for tweet")
    except:
        print("Error connecting to Twitter")
        exit(120)


def twitter_setup(client:tw.StreamingClient, data):
    client.add_rules(tw.StreamRule("from:" + data))


if __name__ == '__main__':
    main()
