import tweepy as tw
import discord_webhook as dw


class ToDiscord(tw.StreamingClient):
    webhook_url = ""
    account_to_follow = ""
    content_message = ""


    # Sets the values that the Twitter API and Discord API need to function
    def set_values(self, data: dict):
        self.webhook_url = data['discord']['webhook_url']
        self.account_to_follow = data['twitter']['account_to_follow']
        self.content_message = data['discord']['content_message']

    # Called when tweepy notices ur tweet owo what's this
    def on_tweet(self, tweet):
        print("Found Tweet, sending to Discord")
        webhook = dw.DiscordWebhook(url=self.webhook_url)
        embed = dw.DiscordEmbed(title=self.content_message, description=tweet.text, color='03b2f8')
        embed.set_url("https://twitter.com/" + self.account_to_follow + "/status/" + str(tweet.id))
        webhook.add_embed(embed)
        response = webhook.execute()
        print(response)
