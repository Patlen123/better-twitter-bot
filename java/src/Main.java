import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

import static javafx.application.Application.launch;

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        String fxmlFile = "java/src/FXML/MainScreen.fxml";
        URL fxmlURL = (new File(fxmlFile).toURI().toURL());
        loader.setLocation(Objects.requireNonNull(fxmlURL));
        Parent root = loader.load();
        primaryStage.setTitle("Hello There");

        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}