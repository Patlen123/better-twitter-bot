package Controllers;
import javafx.fxml.FXML;
import javafx.scene.control.MenuBar;

import java.net.URL;
import java.util.ResourceBundle;

public class MainScreenController {
    @FXML
    MenuBar menuBar;

    @FXML
    public void initialize()    {
        menuBar.setUseSystemMenuBar(true);
    }
}
