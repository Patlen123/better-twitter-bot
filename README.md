# Tweet2Cord #
## Under Construction ##

Tweet2Cord is a bot that will take new tweets posted to Twitter and send them to a Discord channel of your choice

It uses Discord Webhooks and the Twitter API to achieve this

It doesn't do much other than posting one accounts tweets to one channel just yet but I'm working on that

## Usage ##

The bot is configured using a JSON file (an example of which is provided in the repo), full documentation will be provided later

## Libraries Used ##

Tweepy

discord-webhook